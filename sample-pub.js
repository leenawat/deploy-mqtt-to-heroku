var mqtt    = require('mqtt');
var client  = mqtt.connect('ws://localhost:8080');

client.on('connect', function () {
  client.subscribe('presence');
  client.publish('presence', 'Hello mqtt : ' + new Date().toISOString());
  client.end();
});
